import React, { Component } from 'react';

class MemberList extends Component{
    constructor(props){
        super(props)
        // console.log(this.props.members)
    }
    handler=(evt)=>{
        //data-id => dataset.id
        this.props.modify(evt.target.dataset.id, evt.target.dataset.type)
        //傳給merber.js
        evt.preventDefault();
    }
    render(){
        return(
        <React.Fragment>

        <table className="table table-bordered">
            <thead className="thead-light">
                <tr>
                    <th>編號</th>
                    <th>姓名</th>
                    <th>電子郵件</th>
                    <th>年紀</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                {
                    this.props.members.map(member => 
                        <tr key={member.id}>
                            <td>{member.id}</td>
                            <td>{member.name}</td>
                            <td>{member.email}</td>
                            <td>{member.age}</td>
                            <td><button className="btn btn-danger mr-3" data-id={member.id} data-type="del" onClick={this.handler}>DEL</button>
                            <button className="btn btn-info" data-id={member.id} data-type="edit" onClick={this.handler}>EDIT</button></td>
                        </tr>

                    )
                }

            </tbody>
        </table>
        </React.Fragment>
        
        )
    }
}

export default MemberList;