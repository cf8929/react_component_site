import React, { Component } from 'react';

class MemberModify extends Component{
    constructor(props){
        super(props)
        this.initState={
            id:"",
            name:"",
            email:"",
            age:""
        }
        this.state = this.initState
    }
    handleChange = (evt)=>{
        let key = evt.target.id;
        let value =evt.target.value;
        this.setState({
            [key]:value,
        })

    }
    addHandler = (evt) =>{   
        evt.preventDefault();
        //將新增的會員資料傳給父元件 memberAdd => function
        this.props.memberAdd(this.state);
        this.setState(this.initState);
        evt.preventDefault();
     

    }
    updateHandler = (evt) =>{
        evt.preventDefault();

    }
    render(){
        return(
        <React.Fragment>
            <form>
                {/* <div className="form-group">
                <label htmlFor="id">編號</label>
                <input type="text" className="form-control" id="id" placeholder="id" value={this.state.id} onChange={this.handleChange}></input>
                </div> */}
                <div className="form-group">
                <label htmlFor="name">姓名</label>
                <input type="text" className="form-control" id="name" placeholder="姓名" value={this.state.name} onChange={this.handleChange}></input>
                </div>
                <div className="form-group">
                <label htmlFor="email">email</label>
                <input type="email" className="form-control" id="email" placeholder="email" value={this.state.email} onChange={this.handleChange}></input>
                </div>
                <div className="form-group">
                <label htmlFor="age">年紀</label>
                <input type="text" className="form-control" id="age" placeholder="年紀" value={this.state.age} onChange={this.handleChange}></input>
                </div>
                <button className="btn btn-primary mr-3" onClick={this.addHandler}>新增</button>
                <button className="btn btn-secondary" onClick={this.updateHandler}>修改</button>
            </form>
        </React.Fragment>
        
        )
    }
}

export default MemberModify;