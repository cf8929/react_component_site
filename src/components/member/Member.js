import React, { Component } from 'react';
import MemberList from './MemberList'
import MemberModify from './MemberModify';
// import members from './members.json'

class Member extends Component{
    constructor(props){
        super(props)
        this.state={
            myMembers: []
        }
    }
    addHandler =(datas) =>{
        // console.log("member addHandler")
        // console.log(datas)
        //刪除id
        delete datas.id;
        //呼叫restful api 新增資料 POST
        fetch("http://localhost:3000/api/members",{
            method: 'POST',
            body: JSON.stringify(datas),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(res=>res.json()) //{message:新增成功}
        .then(data=>{
            alert(data.message)
            this.getMembers();
        })
    //    this.state.myMembers.push(datas);
    //    let members = this.state.myMembers;
    //    console.log(members)
    //     this.setState({
    //         members:members
    //     })
    }
    modifyHandler = (id,type)=>{
        switch (type){
            case "edit":
            alert('edit');
            break;
            case "del":
             fetch("http://localhost:3000/api/members/" + id ,{
                 method: 'DELETE'
        }).then(res => res.json())
        .then(data =>{
            alert(data.message);
            this.getMembers();
        })
            break;            
        }

    }
    // delHandler =(id) =>{
    //     alert("merberid :"+id)
    //     alert(this.state.myMembers)
    //     const _members = this.state.myMembers.filter(member => parseInt(member.id) !== parseInt(id))
    //     alert(_members)
    //     this.setState({
    //         myMembers:_members
    //     })
    // }

    render(){
        return(
        <React.Fragment>
            <h2>會員頁面</h2>
            <div className="row">
            <div className="col-8"><MemberList members={this.state.myMembers} modify={this.modifyHandler}/></div>
            <div className="col-4"><MemberModify memberAdd={this.addHandler}/></div>
            </div>

        </React.Fragment>
        
        )
    }
    componentDidMount(){
        this.getMembers();
    }

    getMembers(){
        fetch("http://localhost:3000/api/members")
        .then(res=>res.json())
        .then(members=> this.setState({
            myMembers:members
        }))
    }
}

export default Member;