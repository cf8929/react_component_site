import React, { Component } from 'react';
import './Home.css';
import $ from 'jquery';

class Home extends Component{
    constructor(props){
        super(props)
        // this.state={
        //     image:"images/4.png"
        // }
    }
    render(){
        return(
            <React.Fragment>
        <h2 className="title">Home Page</h2>
        <img src="images/4.png" alt="小火龍" id="img1" />
        </React.Fragment>
        )
    }
    //這個事件發生表示網頁上的內容產生
    //就可以透過jQuery做一些效果
    componentDidMount = () =>{
        // console.log("didMount")
        let _this = this;
        var theImg = document.querySelector('#img1');
        theImg.addEventListener("click",function(){
            // _this.setState({
            //     image: "images/7.png"
            // })
            theImg.setAttribute("src","images/7.png")
        })

        //jQuery
        $('#img1').hover(function(){
            $(this).fadeTo(1000,1)
        },function(){
            $(this).fadeTo(1000,.3)
        })

    }
}

export default Home;