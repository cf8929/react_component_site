import React, { Component } from 'react';
import { BrowserRouter, Route, Link} from 'react-router-dom';
import Department from './Department';
import Management from './Management';
import Organization from './Organization';

class About extends Component{
    constructor(props){
        super(props)
        console.log(props.match.url)
    }
    render(){

        return(
        <BrowserRouter>
        <React.Fragment>
        <h2 className="title">About Page</h2>
        <Link to ={`${this.props.match.path}/organization`}> 組織架構 </Link>|
        <Link to ={`${this.props.match.path}/management`}> 經營團隊 </Link>|
        <Link to ={`${this.props.match.path}/department`}> 業務單位 </Link>
        <hr />

        <switch>
        {/*http://localhost:3000/about/organization*/}
        <Route path={`${this.props.match.path}/organization`} component={Organization}/>
        {/*http://localhost:3000/about/management*/}
        <Route path={`${this.props.match.path}/management`} component={Management}/>
        {/*http://localhost:3000/about/department*/}
        <Route path={`${this.props.match.path}/department`} component={Department}/>
        </switch>


        </React.Fragment>
        </BrowserRouter>
        )
    }
}

export default About;