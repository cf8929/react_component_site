import React, { Component } from 'react';
import './home/Home.css'

class Store extends Component{
    constructor(props){
        super(props)
        this.category = props.match.params.category;

    }
    render(){
        return(
         //取代唯一的根元素(div))
        <React.Fragment> 
         <h2 className="title">24小時購物</h2>
         <p>您要瀏覽 {this.category} 類型的商品</p>

        </React.Fragment>

        )
    }
}

export default Store;